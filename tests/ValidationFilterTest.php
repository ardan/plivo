<?php

use Mockery as m;
use Ardan\Plivo\Filters\ValidationFilter;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ValidationFilterTest extends PHPUnit_Framework_TestCase {

  /**
   * Filter class to test
   *
   * @var \Ardan\Plivo\Filters\ValidationFilter
   */
  protected $filter;



  /**
   * Setup the tests
   *
   * @access public
   * @param void
   * @return void
   */
  public function setUp() {

    $xmlResponse = m::mock('\Ardan\Plivo\Response', [ new HttpResponse, '<?xml version="1.0" encoding="utf-8" ?>' ]);
    $xmlResponse->makePartial();
    $xmlResponse->shouldReceive('response')->andReturn('Hangup');

    $this->filter = new ValidationFilter(
      'OTIyOTNhZDNhNTViNWZlNDE2ZTZlMTZhODk5MGI5',
      $xmlResponse
    );

  } /* function setUp */



  /**
   * Tear down
   *
   * @access public
   * @param void
   * @return void
   */
  public function tearDown() {

    m::close();

  } /* function tearDown */



  /**
   * Test that an invalid request returns a hangup
   *
   * @test
   */
  public function bad_request_returns_hangup() {

    $request = m::mock('StdClass');
    $request->shouldReceive('url')->once()->andReturn('');
    $request->shouldReceive('input')->once()->andReturn([]);
    $request->shouldReceive('header')->once()->with('X-Plivo-Signature')->andReturn('');

    $response = $this->filter->filter(null, $request);

    $this->assertEquals('Hangup', $response);

  } /* function bad_request_returns_hangup */



  /**
   * Test that a valid request returns no response
   *
   * @test
   */
  public function valid_request_returns_null_response() {

    $params = [
      'TotalCost' => '0.00000',
      'Direction' => 'inbound',
      'BillDuration' => '0',
      'From' => '17205458310',
      'CallerName' => '+17205458310',
      'HangupCause' => 'USER_BUSY',
      'BillRate' => '0.00850',
      'To' => '16264601834',
      'AnswerTime' => '',
      'StartTime' => '2014-08-03 21:43:35',
      'Duration' => '0',
      'CallUUID' => '38d79fd4-1b57-11e4-bb77-2fd63e118d10',
      'EndTime' => '2014-08-03 21:43:36',
      'CallStatus' => 'busy',
      'Event' => 'Hangup',
    ];

    $request = m::mock('\Illuminate\Http\Request');
    $request->shouldReceive('url')->once()->andReturn('http://white-white-harbor-php-25-104894.use1-2.nitrousbox.com:8080/ring/tollfree/hangup');
    $request->shouldReceive('input')->once()->andReturn($params);
    $request->shouldReceive('header')->once()->with('X-Plivo-Signature')->andReturn('BOkVSAXow/+XsvUXPYVbKovyV2Y=');

    $response = $this->filter->filter(null, $request);

    $this->assertNull($response);

  } /* function valid_request_returns_null_response */

} /* class ValidationFilterTest */

/* EOF */
