# Plivo XML and API for Laravel 4
This Laravel 4 package gives access to both the Plivo RESTful API and the Plivo XML class.

## Installation
First install `ardan/plivo` via composer, then update your `config/app.php` file before publishing the package's configuration files and adding your Plivo Auth ID and Plivo Auth Token.

### Composer
Insall via composer by adding the following code to your root application's `composer.json` file:

```json
"require": {
  "ardan/plivo": "dev-master"
},
"repositories": [
  {
    "type": "git",
    "url": "https://ardan@bitbucket.org/ardan/plivo.git"
  }
]
```
After saving the `composer.json` file, run `composer update` from the command line to download the package.

### Service Provider
Add the following code to the `providers` array in your application's `config/app.php` file:

```php
'Ardan\Plivo\PlivoServiceProvider'
```

### Aliases
Add the following code to the `aliases` array in your application's `config/app.php` file:
```php
'Plivo' => 'Ardan\Plivo\Facades\Plivo'
```

### Config
From the command line, run `php artisan config:publish ardan/plivo`. This will copy the configuration file to `config/packages/ardan/plivo/config.php`. Open this file with your favorite text editor and add your Plivo Auth ID and Plivo Auth Token. You may want to store the token in the `$_ENV` variable by adding an entry to the `.env.php` file located in the root directory of the application.

## Usage
This package provides access to both the Plivo RESTful API and the Plivo XML Responses.

### Plivo API
Call the API with `Plivo::api()`

Example:
```php
Plivo::api()->getAccount();
```

### Plivo XML
You may use the validation filter 'plivo.validate' in your routes to ensure the request is coming from Plivo.

Create a response with `Plivo::xml()`

Get the response with `Plivo::xml()->response()`

Example:
```php
Route::get('answer', [ 'before' => 'plivo.validate', 'uses' => function () {
  
  $xml = Plivo::xml();
  $xml->addSpeak('Thanks for using Laravel!');
  
  return $xml->response();

}]);
```
You can reset a response with `Plivo::xml()->reset()`

## Todo
* Update readme.md
* Unit Testing
* Style Code
