<?php

namespace Ardan\Plivo;

use Exception;
use GuzzleHttp\Client as Guzzle;
use Ardan\Plivo\Errors\PlivoError;
use GuzzleHttp\Exception\ClientException;

class RestAPI {

  /**
   * API URL
   *
   * @var string
   */
  protected $api;

  /**
   * Authorization ID
   *
   * @var string
   */
  protected $authId;

  /**
   * Authorization token
   *
   * @var string
   */
  protected $authToken;

  /**
   * HTTP Client
   *
   * @var GuzzleHttp\Client
   */
  private $client;



  /**
   * Constructor
   *
   * @access public
   * @param string $version
   * @param string $authID
   * @param string $authToken
   * @param string $url
   * @param \GuzzleHttp\Client $client
   * @return void
   */
  public function __construct(
    $version,
    $authId,
    $authToken,
    $url,
    Guzzle $client
  ) {

    $this->authId = $authId;
    $this->authToken = $authToken;
    $this->api = "$url/{$version}/Account/{$this->authId}";
    $this->client = $client;

  } /* function __construct */



  /**
   * Send a synchronous request to the API
   *
   * @param string $method
   * @param string $path
   * @param array [$params]
   */
  private function request($method, $path, $params=array()) {

    // Create the complete URL
    $url = $this->api.rtrim($path, '/').'/';

    // Setup our default options
    $options = array(
      'headers' => array(
        'User-Agent' => 'PHPPlivo',
        'Connection' => 'close',
      ),
      'auth' => array(
        $this->authId,
        $this->authToken
      ),
      'timeout' => '20',
      'verify' => false,
    );

    switch ( $method ) {

      case "POST":
        // Pass paramters as content
        // Content-Type: application/json
        if ( $params )
          $options['body'] = json_encode($params);
        $options['headers']['Content-Type'] = 'application/json';
        break;

      case "DELETE":
      case "GET":
        // Pass parameters as url encoded query string
        if ( $params )
          $options['query'] = $params;
        break;

      default:
        throw new PlivoError("Bad HTTP method: {$method}");

    }

    // Create and send the request
    try {

      $request = $this->client->createRequest($method, $url, $options);
      $response = $this->client->send($request);
      $status = $response->getStatusCode();
      $body = json_decode($response->getBody(), true);

    } catch ( ClientException $e ) {

      $response = $e->getResponse();
      $status = $response->getStatusCode();
      $body = $response->getReasonPhrase();

    } catch ( Exception $e ) {

      $status = $e->getCode();
      $body = $e->getMessage();

    }

    return array(
      'status' => $status,
      'response' => $body
    );

  } /* function request */



  private function pop($params, $key) {
      $val = $params[$key];
      if (!$val) {
          throw new PlivoError($key." parameter not found");
      }
      unset($params[$key]);
      return $val;
  }

  ## Accounts ##
  public function getAccount($params=array()) {
      return $this->request('GET', '', $params);
  }

  public function modifyAccount($params=array()) {
      return $this->request('POST', '', $params);
  }

  public function getSubaccounts($params=array()) {
      return $this->request('GET', '/Subaccount/', $params);
  }

  public function createSubaccount($params=array()) {
      return $this->request('POST', '/Subaccount/', $params);
  }

  public function getSubaccount($params=array()) {
      $subauthId = $this->pop($params, "subauthId");
      return $this->request('GET', '/Subaccount/'.$subauthId.'/', $params);
  }

  public function modifySubaccount($params=array()) {
      $subauthId = $this->pop($params, "subauthId");
      return $this->request('POST', '/Subaccount/'.$subauthId.'/', $params);
  }

  public function deleteSubaccount($params=array()) {
      $subauthId = $this->pop($params, "subauthId");
      return $this->request('DELETE', '/Subaccount/'.$subauthId.'/', $params);
  }

  ## Applications ##
  public function getApplications($params=array()) {
      return $this->request('GET', '/Application/', $params);
  }

  public function createApplication($params=array()) {
      return $this->request('POST', '/Application/', $params);
  }

  public function getApplication($params=array()) {
      $app_id = $this->pop($params, "app_id");
      return $this->request('GET', '/Application/'.$app_id.'/', $params);
  }

  public function modifyApplication($params=array()) {
      $app_id = $this->pop($params, "app_id");
      return $this->request('POST', '/Application/'.$app_id.'/', $params);
  }

  public function deleteApplication($params=array()) {
      $app_id = $this->pop($params, "app_id");
      return $this->request('DELETE', '/Application/'.$app_id.'/', $params);
  }

  ## Numbers ##
  public function getNumbers($params=array()) {
      return $this->request('GET', '/Number/', $params);
  }

 ## This API is available only for US numbers with some limitations ##
 ## Please use getNumberGroup and rentFromNumberGroup instead ##
  public function searchNumbers($params=array()) {
      return $this->request('GET', '/AvailableNumber/', $params);
  }

  public function getNumber($params=array()) {
      $number = $this->pop($params, "number");
      return $this->request('GET', '/Number/'.$number.'/', $params);
  }

  ## This API is available only for US numbers with some limitations ##
 ## Please use getNumberGroup and rentFromNumberGroup instead ##
  public function rentNumber($params=array()) {
      $number = $this->pop($params, "number");
      return $this->request('POST', '/AvailableNumber/'.$number.'/');
  }

  public function unrentNumber($params=array()) {
      $number = $this->pop($params, "number");
      return $this->request('DELETE', '/Number/'.$number.'/', $params);
  }

  public function linkApplicationNumber($params=array()) {
      $number = $this->pop($params, "number");
      return $this->request('POST', '/Number/'.$number.'/', $params);
  }

  public function unlinkApplicationNumber($params=array()) {
      $number = $this->pop($params, "number");
      $params = array("app_id" => "");
      return $this->request('POST', '/Number/'.$number.'/', $params);
  }

  public function getNumberGroup($params=array()) {
      return $this->request('GET', '/AvailableNumberGroup/', $params);
  }

  public function getNumberGroupDetails($params=array()) {
      $group_id = $this->pop($params, "group_id");
      return $this->request('GET', '/AvailableNumberGroup/'.$group_id.'/', $params);
  }

  public function rentFromNumberGroup($params=array()) {
      $group_id = $this->pop($params, "group_id");
      return $this->request('POST', '/AvailableNumberGroup/'.$group_id.'/', $params);
  }

  ## Calls ##
  public function getCdrs($params=array()) {
      return $this->request('GET', '/Call/', $params);
  }

  public function getCdr($params=array()) {
      $record_id = $this->pop($params, 'record_id');
      return $this->request('GET', '/Call/'.$record_id.'/', $params);
  }

  public function getLiveCalls($params=array()) {
      $params["status"] = "live";
      return $this->request('GET', '/Call/', $params);
  }

  public function getLiveCall($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      $params["status"] = "live";
      return $this->request('GET', '/Call/'.$call_uuid.'/', $params);
  }

  public function makeCall($params=array()) {
      return $this->request('POST', '/Call/', $params);
  }

  public function hangupAllCalls($params=array()) {
      return $this->request('DELETE', '/Call/', $params);
  }

  public function transferCall($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('POST', '/Call/'.$call_uuid.'/', $params);
  }

  public function hangupCall($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('DELETE', '/Call/'.$call_uuid.'/', $params);
  }

  public function record($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('POST', '/Call/'.$call_uuid.'/Record/', $params);
  }

  public function stopRecord($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('DELETE', '/Call/'.$call_uuid.'/Record/', $params);
  }

  public function play($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('POST', '/Call/'.$call_uuid.'/Play/', $params);
  }

  public function stopPlay($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('DELETE', '/Call/'.$call_uuid.'/Play/', $params);
  }

  public function speak($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('POST', '/Call/'.$call_uuid.'/Speak/', $params);
  }

  public function stopSpeak($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('DELETE', '/Call/'.$call_uuid.'/Speak/', $params);
  }

  public function sendDigits($params=array()) {
      $call_uuid = $this->pop($params, 'call_uuid');
      return $this->request('POST', '/Call/'.$call_uuid.'/DTMF/', $params);
  }

  ## Calls requests ##
  public function hangupRequest($params=array()) {
      $request_uuid = $this->pop($params, 'request_uuid');
      return $this->request('DELETE', '/Request/'.$request_uuid.'/', $params);
  }

  ## Conferences ##
  public function getLiveConferences($params=array()) {
      return $this->request('GET', '/Conference/', $params);
  }

  public function hangupAllConferences($params=array()) {
      return $this->request('DELETE', '/Conference/', $params);
  }

  public function getLiveConference($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      return $this->request('GET', '/Conference/'.$conference_name.'/', $params);
  }

  public function hangupConference($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      return $this->request('DELETE', '/Conference/'.$conference_name.'/', $params);
  }

  public function hangupMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('DELETE', '/Conference/'.$conference_name.'/Member/'.$member_id.'/', $params);
  }

  public function playMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('POST', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Play/', $params);
  }

  public function stopPlayMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('DELETE', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Play/', $params);
  }

  public function speakMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('POST', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Speak/', $params);
  }

  public function deafMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('POST', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Deaf/', $params);
  }

  public function undeafMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('DELETE', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Deaf/', $params);
  }

  public function muteMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('POST', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Mute/', $params);
  }

  public function unmuteMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('DELETE', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Mute/', $params);
  }

  public function kickMember($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      $member_id = $this->pop($params, 'member_id');
      return $this->request('POST', '/Conference/'.$conference_name.'/Member/'.$member_id.'/Kick/', $params);
  }

  public function recordConference($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      return $this->request('POST', '/Conference/'.$conference_name.'/Record/', $params);
  }

  public function stopRecordConference($params=array()) {
      $conference_name = $this->pop($params, 'conference_name');
      $conference_name = rawurlencode($conference_name);
      return $this->request('DELETE', '/Conference/'.$conference_name.'/Record/', $params);
  }

  ## Recordings ##
  public function getRecordings($params=array()) {
      return $this->request('GET', '/Recording/', $params);
  }

  public function getRecording($params=array()) {
      $recording_id = $this->pop($params, 'recording_id');
      return $this->request('GET', '/Recording/'.$recording_id.'/', $params);
  }

  ## Endpoints ##
  public function getEndpoints($params=array()) {
      return $this->request('GET', '/Endpoint/', $params);
  }

  public function createEndpoint($params=array()) {
      return $this->request('POST', '/Endpoint/', $params);
  }

  public function getEndpoint($params=array()) {
      $endpoint_id = $this->pop($params, 'endpoint_id');
      return $this->request('GET', '/Endpoint/'.$endpoint_id.'/', $params);
  }

  public function modifyEndpoint($params=array()) {
      $endpoint_id = $this->pop($params, 'endpoint_id');
      return $this->request('POST', '/Endpoint/'.$endpoint_id.'/', $params);
  }

  public function deleteEndpoint($params=array()) {
      $endpoint_id = $this->pop($params, 'endpoint_id');
      return $this->request('DELETE', '/Endpoint/'.$endpoint_id.'/', $params);
  }

  ## Incoming Carriers ##
  public function getIncomingCarriers($params=array()) {
      return $this->request('GET', '/IncomingCarrier/', $params);
  }

  public function createIncomingCarrier($params=array()) {
      return $this->request('POST', '/IncomingCarrier/', $params);
  }

  public function getIncomingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('GET', '/IncomingCarrier/'.$carrier_id.'/', $params);
  }

  public function modifyIncomingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('POST', '/IncomingCarrier/'.$carrier_id.'/', $params);
  }

  public function deleteIncomingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('DELETE', '/IncomingCarrier/'.$carrier_id.'/', $params);
  }

  ## Outgoing Carriers ##
  public function getOutgoingCarriers($params=array()) {
      return $this->request('GET', '/OutgoingCarrier/', $params);
  }

  public function create_outgoing_carrier($params=array()) {
      return $this->request('POST', '/OutgoingCarrier/', $params);
  }

  public function getOutgoingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('GET', '/OutgoingCarrier/'.$carrier_id.'/', $params);
  }

  public function modifyOutgoingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('POST', '/OutgoingCarrier/'.$carrier_id.'/', $params);
  }

  public function delete0utgoingCarrier($params=array()) {
      $carrier_id = $this->pop($params, 'carrier_id');
      return $this->request('DELETE', '/OutgoingCarrier/'.$carrier_id.'/', $params);
  }

  ## Outgoing Carrier Routings ##
  public function getOutgoingCarrierRoutings($params=array()) {
      return $this->request('GET', '/OutgoingCarrierRouting/', $params);
  }

  public function create_outgoing_carrier_routing($params=array()) {
      return $this->request('POST', '/OutgoingCarrierRouting/', $params);
  }

  public function getOutgoingCarrierRouting($params=array()) {
      $routing_id = $this->pop($params, 'routing_id');
      return $this->request('GET', '/OutgoingCarrierRouting/'.$routing_id.'/', $params);
  }

  public function modifyOutgoingCarrierRouting($params=array()) {
      $routing_id = $this->pop($params, 'routing_id');
      return $this->request('POST', '/OutgoingCarrierRouting/'.$routing_id.'/', $params);
  }

  public function deleteOutgoingCarrierRouting($params=array()) {
      $routing_id = $this->pop($params, 'routing_id');
      return $this->request('DELETE', '/OutgoingCarrierRouting/'.$routing_id.'/', $params);
  }

  ## Pricing ##
  public function pricing($params=array()) {
      return $this->request('GET', '/Pricing/', $params);
  }

  ## Outgoing Carriers ##

  ## To be added here ##

  ## Message ##
  public function sendMessage($params=array()) {
      return $this->request('POST', '/Message/', $params);
  }

  public function getMessages($params=array()) {
      return $this->request('GET', '/Message/', $params);
  }

  public function getMessage($params=array()) {
      $record_id = $this->pop($params, 'record_id');
      return $this->request('GET', '/Message/'.$record_id.'/', $params);
  }

} /* class RestAPI */

/* EOF */
