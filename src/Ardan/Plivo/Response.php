<?php

namespace Ardan\Plivo;

use Ardan\Plivo\Element;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class Response extends Element {

  /**
   * Nestable elements
   *
   * @access protected
   * @var array
   */
  protected $nestables = [
    'Speak', 'Play', 'GetDigits', 'Record',
    'Dial', 'Redirect', 'Wait', 'Hangup',
    'PreAnswer', 'Conference', 'DTMF', 'Message'
  ];

  /**
   * HTTP Response
   *
   * @access protected
   * @var Illuminate\Http\Response
   */
  protected $response;

  /**
   * XML Header
   *
   * @access protected
   * @var string
   */
  protected $xmlHeader;



  /**
   * Constructor
   *
   * @access public
   * @param \Ardan\Plivo\Response $response
   * @param string $xmlHeader
   * @return void
   */
  public function __construct(
    HttpResponse $response,
    $xmlHeader
  ) {

    parent::__construct(null);

    $this->response = $response;
    $this->xmlHeader = $xmlHeader;

  } /* function __construct */



  /**
   * Return the response as an HTTP response
   *
   * @access public
   * @param void
   * @return \Illuminate\Http\Response
   */
  public function response() {

    $content = $this->toXML($this->xmlHeader);

    $this->response->setContent($content);
    $this->response->header('Content-Type', 'text/xml');

    return $this->response;

  } /* function sendResponse */



  /**
   * Reset the Response element
   *
   * @access public
   * @param void
   * @return string
   */
  public function reset() {

    $this->childs = [];

  } /* function reset */

} /* class Response */

/* EOF */
