<?php

namespace Ardan\Plivo;

use Ardan\Plivo\Elements\Conference;
use Ardan\Plivo\Elements\Dial;
use Ardan\Plivo\Elements\DTMF;
use Ardan\Plivo\Elements\GetDigits;
use Ardan\Plivo\Elements\Hangup;
use Ardan\Plivo\Elements\Message;
use Ardan\Plivo\Elements\Number;
use Ardan\Plivo\Elements\Play;
use Ardan\Plivo\Elements\PreAnswer;
use Ardan\Plivo\Elements\Record;
use Ardan\Plivo\Elements\Redirect;
use Ardan\Plivo\Elements\Speak;
use Ardan\Plivo\Elements\User;
use Ardan\Plivo\Elements\Wait;

class Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array();

 /**
  * Element attributes
  *
  * @access protected
  * @var array
  */
  protected $attributes = array();

 /**
  * Element name
  *
  * @access protected
  * @var string
  */
  protected $name = '';

 /**
  * Element body
  *
  * @access protected
  * @var string
  */
  protected $body = NULL;

 /**
  * Child elements
  *
  * @access protected
  * @var array
  */
  protected $childs = array();



 /**
  * Constructor
  *
  * @access public
  * @param string
  * @param array
  * @return void
  */
  function __construct($body='', $attributes=array()) {

    $this->attributes = $attributes;

    if ((!$attributes) || ($attributes === null))
      $this->attributes = array();

    // Remove namespaces from class name.
    $this->name = substr(get_class($this), strrpos(get_class($this), '\\') + 1);
    $this->body = $body;

    foreach ($this->attributes as $key => $value) {

      if ( ! in_array($key, $this->valid_attributes) )
        throw new PlivoError("invalid attribute ".$key." for ".$this->name);

      $this->attributes[$key] = $this->convertValue($value);

    }

  } /* function __construct */


 /**
  * Convert php data types and strings to Plivo strings
  *
  * @access protected
  * @param string
  * @return string
  */
  protected function convertValue($v) {

    if ( $v === TRUE )
      return "true";

    if ( $v === FALSE )
      return "false";

    if ( $v === NULL )
      return "none";

    if ( $v === "get" )
      return "GET";

    if ( $v === "post" )
      return "POST";

    return $v;

  } /* function convertValue */



 /**
  * Add a Speak element
  *
  * @access public
  * @param string
  * @param array
  * @return Speak
  */
  public function addSpeak($body=NULL, $attributes=array()) {

    return $this->add(new Speak($body, $attributes));

  } /* function addSpeak */



 /**
  * Add a Play element
  *
  * @access public
  * @param string
  * @param array
  * @return Play
  */
  public function addPlay($body=NULL, $attributes=array()) {

    return $this->add(new Play($body, $attributes));

  } /* function addPlay */



 /**
  * Add a Dial element
  *
  * @access public
  * @param string
  * @param array
  * @return Dial
  */
  public function addDial($body=NULL, $attributes=array()) {

    return $this->add(new Dial($body, $attributes));

  } /* function addDial */



 /**
  * Add a Number element
  *
  * @access public
  * @param string
  * @param array
  * @return Number
  */
  public function addNumber($body=NULL, $attributes=array()) {

    return $this->add(new Number($body, $attributes));

  } /* function addNumber */



 /**
  * Add a User element
  *
  * @access public
  * @param string
  * @param array
  * @return User
  */
  public function addUser($body=NULL, $attributes=array()) {

    return $this->add(new User($body, $attributes));

  } /* function addUser */



 /**
  * Add a GetDigits element
  *
  * @access public
  * @param string
  * @param array
  * @return GetDigits
  */
  public function addGetDigits($attributes=array()) {

    return $this->add(new GetDigits($attributes));

  } /* function addGetDigits */



 /**
  * Add a Record element
  *
  * @access public
  * @param string
  * @param array
  * @return Record
  */
  public function addRecord($attributes=array()) {

    return $this->add(new Record($attributes));

  } /* function addRecord */



 /**
  * Add a Hangup element
  *
  * @access public
  * @param string
  * @param array
  * @return Hangup
  */
  public function addHangup($attributes=array()) {

    return $this->add(new Hangup($attributes));

  } /* function addHangup */



 /**
  * Add a Redirect element
  *
  * @access public
  * @param string
  * @param array
  * @return Redirect
  */
  public function addRedirect($body=NULL, $attributes=array()) {

    return $this->add(new Redirect($body, $attributes));

  } /* function addRedirect */



 /**
  * Add a Wait element
  *
  * @access public
  * @param string
  * @param array
  * @return Wait
  */
  public function addWait($attributes=array()) {

    return $this->add(new Wait($attributes));

  } /* function addWait*/



 /**
  * Add a Conference element
  *
  * @access public
  * @param string
  * @param array
  * @return Conference
  */
  public function addConference($body=NULL, $attributes=array()) {

    return $this->add(new Conference($body, $attributes));

  } /* function addConference */



 /**
  * Add a PreAnswer element
  *
  * @access public
  * @param array
  * @return
  */
  public function addPreAnswer($attributes=array()) {

    return $this->add(new PreAnswer($attributes));

  } /* function addPreAnswer */



 /**
  * Add a Message element
  *
  * @access public
  * @param string
  * @param array
  * @return Message
  */
  public function addMessage($body=NULL, $attributes=array()) {

    return $this->add(new Message($body, $attributes));

  } /* function addMessage */



 /**
  * Add a DTMF element
  *
  * @access public
  * @param string
  * @param array
  * @return DTMF
  */
  public function addDTMF($body=NULL, $attributes=array()) {

    return $this->add(new DTMF($body, $attributes));

  } /* function addDTMF */



 /**
  * Return the name of the element
  *
  * @access public
  * @param void
  * @return string
  */
  public function getName() {

    return $this->name;

  } /* function getName */



 /**
  * Add an element to the Response
  *
  * @access protected
  * @param Element
  * @return Element
  */
  protected function add($element) {

    if ( ! in_array($element->getName(), $this->nestables) )
      throw new PlivoError($element->getName()." not nestable in ".$this->getName());

    $this->childs[] = $element;

    return $element;

  } /* function add */



 /**
  * Add an attribute to the XML element
  *
  * @access public
  * @param \SimpleXMLElement
  * @return void
  */
  public function setAttributes($xml) {

    foreach ( $this->attributes as $key => $value )
      $xml->addAttribute($key, $value);

  } /* function setAttributes */



 /**
  * Process child elements
  *
  * @access public
  * @param array
  * @return void
  */
  public function asChild($xml) {

    if ( $this->body ) {
      $child_xml = $xml->addChild($this->getName(), htmlspecialchars($this->body));
    } else {
      $child_xml = $xml->addChild($this->getName());
    }

    $this->setAttributes($child_xml);

    foreach ( $this->childs as $child ) {
      $child->asChild($child_xml);
    }

  } /* function asChild */



 /**
  * Return the Element object as XML
  *
  * @access public
  * @param string
  * @return string
  */
  public function toXML($header='') {

    if ( ! isset($xmlstr) )
        $xmlstr = '';

    if ($this->body) {
      $xmlstr .= "<".$this->getName().">".htmlspecialchars($this->body)."</".$this->getName().">";
    } else {
      $xmlstr .= "<".$this->getName()."></".$this->getName().">";
    }

    if ( $header )
      $xmlstr = $header.$xmlstr;

    $xml = new \SimpleXMLElement($xmlstr);
    $this->setAttributes($xml);

    foreach ( $this->childs as $child )
      $child->asChild($xml);

    return $xml->asXML();

  } /* function toXML */



 /**
  * Magic method calls toXML
  *
  * @access public
  * @param void
  * @return string
  */
  public function __toString() {

    return $this->toXML();

  } /* function __tostring */

} /* class Element */

/* EOF */
