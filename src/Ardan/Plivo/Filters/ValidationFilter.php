<?php

namespace Ardan\Plivo\Filters;

use Ardan\Plivo\Response as XmlResponse;

class ValidationFilter {

  /**
   * Auth Token
   *
   * @var string
   */
  protected $authToken;

  /**
   * XML Response
   *
   * @var \Ardan\Plivo\Response
   */
  protected $xml;



  /**
   * Constructor
   *
   * @access public
   * @param string $authToken
   * @param \Ardan\Plivo\Response $xml
   * @return void
   */
  public function __construct(
    $authToken,
    XmlResponse $xml
  ) {

    $this->authToken = $authToken;
    $this->xml = $xml;

  } /* function __construct */



  /**
   * Validate the request is from Plivo
   *
   * @access public
   * @param string $route
   * @param \Symfony\Component\Http\Request $request
   * @return null|\Illuminate\Http\Response
   */
  public function filter($route, $request) {

    $url = $request->url();
    $params = $request->input();
    $header = $request->header('X-Plivo-Signature');

    if ( ! $this->validateSignature($url, $params, $header) )
      return $this->hangup();

  } /* function filter */



  /**
   * Each request to our application should be validated that it came from Plivo
   * Best to validate the request before acting on the request, rather than
   * before we send a response.
   *
   * @access protected
   * @param string $url URL without query string
   * @param array $params
   * @param string $signature X-Plivo-Signature
   * @return bool
   */
  protected function validateSignature($uri, array $params, $signature) {

    ksort($params);

    foreach($params as $key => $value)
      $uri .= "$key$value";

    $generatedSignature = base64_encode(hash_hmac("sha1", $uri, $this->authToken, true));

    return $generatedSignature == $signature;

  } /* function validateSignature */



  /**
   * Invalid request: hangup
   *
   * @access protected
   * @param void
   * @return \Illuminate\Http\Response
   */
  protected function hangup() {

    $this->xml->addHangup();

    return $this->xml->response();

  } /* function hangup */

} /* class ValidationFilter */

/* EOF */
