<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;

class Dial extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array('Number', 'User');

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array(
    'action','method','timeout','hangupOnStar',
    'timeLimit','callerId', 'callerName', 'confirmSound',
    'dialMusic', 'confirmKey', 'redirect',
    'callbackUrl', 'callbackMethod', 'digitsMatch',
    'sipHeaders',
  );



 /**
  * Constructor
  *
  * @access public
  * @param array
  * @return void
  */
  function __construct($attributes=array()) {

    parent::__construct(NULL, $attributes);

  } /* function __construct */

} /* class Dial */

/* EOF */
