<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Message extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array('src', 'dst', 'type', 'callbackMethod', 'callbackUrl');



 /**
  * Constructor
  *
  * @access public
  * @param string
  * @param array
  * @return void
  */
  function __construct($body, $attributes=array()) {

    parent::__construct($body, $attributes);

    if ( ! $body )
      throw new PlivoError("No text set for ".$this->getName());

  } /* function __construct */

} /* class Message */

/* EOF */
