<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Hangup extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array('schedule', 'reason');



 /**
  * Constructor
  *
  * @access public
  * @param array
  * @return void
  */
  function __construct($attributes=array()) {

    parent::__construct(NULL, $attributes);

  } /* function __construct */

} /* class Hangup */

/* EOF */
