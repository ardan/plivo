<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Record extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array(
    'action', 'method', 'timeout','finishOnKey',
    'maxLength', 'playBeep', 'recordSession',
    'startOnDialAnswer', 'redirect', 'fileFormat',
    'callbackUrl', 'callbackMethod',
    'transcriptionType', 'transcriptionUrl',
    'transcriptionMethod',
  );



 /**
  * Constructor
  *
  * @access public
  * @param array
  * @return void
  */
  function __construct($attributes=array()) {

    parent::__construct(NULL, $attributes);

  } /* function __construct */

} /* class Record */

/* EOF */
