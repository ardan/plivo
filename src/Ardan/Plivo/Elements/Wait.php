<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Wait extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array(
    'length', 'silence', 'min_silence',
    'minSilence', 'beep',
  );



 /**
  * Constructor
  *
  * @access public
  * @param array
  * @return void
  */
  function __construct($attributes=array()) {

    parent::__construct(NULL, $attributes);

  } /* function __construct */

} /* class Wait */

/* EOF */
