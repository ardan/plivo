<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Play extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();


 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array('loop');

 /**
  * Constructor
  *
  * @access public
  * @param string
  * @param array
  * @return void
  */
  function __construct($body, $attributes=array()) {

    parent::__construct($body, $attributes);

      if ( ! $body )
        throw new PlivoError("No url set for ".$this->getName());

  } /* function __construct */

} /* class Play */

/* EOF */
