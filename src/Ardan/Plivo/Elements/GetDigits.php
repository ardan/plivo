<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class GetDigits extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array('Speak', 'Play', 'Wait');

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array(
    'action', 'method', 'timeout', 'digitTimeout',
    'numDigits', 'retries', 'invalidDigitsSound',
    'validDigits', 'playBeep', 'redirect', 'finishOnKey',
    'digitTimeout', 'log',
  );



 /**
  * Constructor
  *
  * @access public
  * @param array
  * @return void
  */
  public function __construct($attributes=array()) {

      parent::__construct(NULL, $attributes);

  } /* function __construct */

} /* class GetDigits */

/* EOF */
