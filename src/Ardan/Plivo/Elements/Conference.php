<?php

namespace Ardan\Plivo\Elements;

use Ardan\Plivo\Element;
use Ardan\Plivo\Errors\PlivoError;

class Conference extends Element {

 /**
  * Nestable elements
  *
  * @access protected
  * @var array
  */
  protected $nestables = array();

 /**
  * Valid element attributes
  *
  * @access protected
  * @var array
  */
  protected $valid_attributes = array(
    'muted','beep','startConferenceOnEnter',
    'endConferenceOnExit','waitSound','enterSound', 'exitSound',
    'timeLimit', 'hangupOnStar', 'maxMembers',
    'record', 'recordFileFormat', 'action', 'method', 'redirect',
    'digitsMatch', 'callbackUrl', 'callbackMethod',
    'stayAlone', 'floorEvent',
    'transcriptionType', 'transcriptionUrl',
    'transcriptionMethod', 'relayDTMF',
  );



 /**
  * Constructor
  *
  * @access public
  * @param string
  * @param array
  * @return void
  */
  function __construct($body, $attributes=array()) {

    parent::__construct($body, $attributes);

    if ( ! $body )
      throw new PlivoError("No conference name set for ".$this->getName());

  } /* function __construct */

} /* class Conference */

/* EOF */
