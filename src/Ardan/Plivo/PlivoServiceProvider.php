<?php namespace Ardan\Plivo;

use Illuminate\Support\ServiceProvider;
use Ardan\Plivo\Filters\ValidationFilter;

class PlivoServiceProvider extends ServiceProvider {

 /**
  * Indicates if loading of the provider is deferred.
  *
  * @var bool
  */
  protected $defer = false;



 /**
  * Boot the service provider.
  *
  * @access public
  * @param void
  * @return void
  */
  public function boot() {

    $this->package('ardan/plivo', 'plivo');

  } /* function boot */



 /**
  * Register the service provider.
  *
  * @access public
  * @param void
  * @return void
  */
  public function register() {

    $this->registerResponse();
    $this->registerRestAPI();
    $this->registerPlivo();
    $this->registerValidationFilter();

  } /* function register */



 /**
  * Register the Plivo Api class.
  *
  * @access public
  * @param void
  * @return void
  */
  public function registerRestAPI() {

    $this->app->bind('Ardan\Plivo\RestAPI', function ($app) {

      return new RestAPI(
        $app['config']['plivo::version'],
        $app['config']['plivo::auth_id'],
        $app['config']['plivo::auth_token'],
        $app['config']['plivo::url'],
        new \GuzzleHttp\Client
      );

    });

  } /* function registerRestAPI */



 /**
  * Register the Response service provider.
  *
  * @access public
  * @param void
  * @return void
  */
  public function registerResponse() {

    $this->app->bind('Ardan\Plivo\Response', function ($app) {

      return new Response(
        new \Illuminate\Http\Response,
        $app['config']['plivo::xml_header']
      );

    });

  } /* function registerResponse */



 /**
  * Register the Plivo service provider.
  *
  * @access public
  * @param void
  * @return void
  */
  public function registerPlivo() {

    $this->app['plivo'] = $this->app->share(function ($app) {

      return new Plivo(
        $app->make('Ardan\Plivo\Response'),
        $app->make('Ardan\Plivo\RestAPI')
      );

    });

  } /* function registerPlivo */



  /**
   * Register the validation class filter
   *
   * @access protected
   * @param void
   * @return void
   */
  protected function registerValidationFilter() {

    $this->app->bind('Ardan\Plivo\Filters\ValidationFilter', function ($app) {

      return new ValidationFilter(
        $app['config']['plivo::auth_token'],
        $app->make('Ardan\Plivo\Response')
      );

    });

    $this->app['router']->filter('plivo.validate', 'Ardan\Plivo\Filters\ValidationFilter');

  } /* function registerValidationFilter */



 /**
  * Get the services provided by the provider.
  *
  * @access public
  * @param void
  * @return array
  */
  public function provides() {

    return [ 'plivo' ];

  } /* function provides */ 
} /* class PlivoServiceProvider */

/* EOF */
