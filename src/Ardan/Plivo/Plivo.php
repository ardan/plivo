<?php

namespace Ardan\Plivo;

use Ardan\Plivo\Errors\PlivoError;

class Plivo {

 /**
  * Plivo API
  *
  * @access private
  * @var \Ardan\Plivo\RestAPI
  */
  private $api;

 /**
  * Plivo XML
  *
  * @access private
  * @var \Ardan\Plivo\Response
  */
  private $xml;



 /**
  * Constructor
  *
  * @access public
  * @param \Ardan\Plivo\Response
  * @param \Ardan\Plivo\RestAPI
  * @return void
  */
  public function __construct(
    Response $xml,
    RestAPI $api
  ) {

    $this->xml = $xml;
    $this->api = $api;

  } /* function __construct */



 /**
  * Set the Auth Token
  *
  * @access public
  * @param string
  * @return void
  */
  public function setAuthToken($authToken) {

    $this->xml->setAuthToken($authToken);
    $this->api->setAuthToken($authToken);

  } /* function setAuthToken */



 /**
  * Set the Auth ID
  *
  * @access public
  * @param string
  * @return void
  */
  public function setAuthId($authId) {

    $this->xml->setAuthId($authId);
    $this->api->setAuthId($authId);

  } /* function setAuthId */


 /**
  * Return the Response class
  *
  * @access public
  * @param void
  * @return \Ardan\Plivo\Response
  */
  public function xml() {

    return $this->xml;

  } /* function xml */



 /**
  * Return the PlivoAPI class
  *
  * @access public
  * @param void
  * @return \Ardan\Plivo\RestAPI
  */
  public function api() {

    return $this->api;

  } /* function api */

} /* class Plivo */

/* EOF */
