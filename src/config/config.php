<?php

return array(

  /**
   * Auth ID from Plivo
   */
  'auth_id' => '',

  /**
   * Auth Token from Plivo
   */
  'auth_token' => '',

  /**
   * API URL
   */
  'url' => 'https://api.plivo.com',

  /**
   * API Version
   */
  'version' => 'v1',

  /**
   * XML header to use with the response
   */
  'xml_header' => '<?xml version="1.0" encoding="utf-8" ?>',

);

/* EOF */
